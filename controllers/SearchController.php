<?php

namespace PixelHumain\PixelHumain\modules\communecter\controllers;

use CommunecterController;

/**
 * DefaultController.php
 *
 * OneScreenApp for Communecting people
 *
 * @author: Tibor Katelbach <tibor@pixelhumain.com>
 * Date: 14/03/2014
 */
class SearchController extends CommunecterController {

  public function beforeAction($action) {
      return parent::beforeAction($action);
  }

  public function actions()
  {
      return array(
          'globalautocomplete'      	=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\search\GlobalAutoCompleteAction::class,
          'simplyautocomplete'        => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\search\SimplyAutoCompleteAction::class,
          'searchmemberautocomplete'  => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\search\SearchMembersAutoCompleteAction::class,
          'getshortdetailsentity'     => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\search\GetShortDetailsEntityAction::class,
          'searchbycriteria'          => 'citizenToolKit.controllers.search.SearchByCriteriaAction',
          'index'                     => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\search\IndexAction::class,
          'mainmap'                   => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\search\MainMapAction::class,
      );
  }
}
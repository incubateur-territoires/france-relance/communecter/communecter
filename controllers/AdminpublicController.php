<?php

namespace PixelHumain\PixelHumain\modules\communecter\controllers;

use CommunecterController;

/**
 * 
 *
 * @author: Raphael RIVIERE
 * Date:
 */
class AdminpublicController extends CommunecterController {

  public function beforeAction($action)
  {
	parent::initPage();
	return parent::beforeAction($action);
  }

	public function actions()
	{
		return array(
		// captcha action renders the CAPTCHA image displayed on the contact page
		'index'   				=> 'citizenToolKit.controllers.adminpublic.IndexAction',
		'createfile' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\adminpublic\CreateFileAction::class,
		'adddata' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\adminpublic\AddDataAction::class,
	    'adddataindb' => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\adminpublic\AddDataInDbAction::class,
	    'sourceadmin' => 'citizenToolKit.controllers.adminpublic.SourceadminAction',
		);
	}	
}
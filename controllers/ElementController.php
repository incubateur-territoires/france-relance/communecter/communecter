<?php

namespace PixelHumain\PixelHumain\modules\communecter\controllers;

use CommunecterController;

/**
 * ElementController.php
 *
 * @author: Tibor Katelbach <tibor@pixelhumain.com>
 * Date: 15/08/13
 */
class ElementController extends CommunecterController {
    const moduleTitle = "Element";
    
  public function beforeAction($action) {
    parent::initPage();
    return parent::beforeAction($action);
  }
  public function actions()
  {
      return array(
          'updatefield' 				  => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\element\UpdateFieldAction::class,
          'updatefields' 				  => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\element\UpdateFieldsAction::class,
          'updatesettings'        => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\element\UpdateSettingsAction::class,
          'detail'                => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\element\DetailAction::class,
          'getalllinks'           => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\element\GetAllLinksAction::class,
          'directory'             => 'citizenToolKit.controllers.element.DirectoryAction',
          'addmembers'            => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\element\AddMembersAction::class,
          'aroundme'              => 'citizenToolKit.controllers.element.AroundMeAction',
          'updatefield'           => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\element\UpdateFieldAction::class,
          'save'                  => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\element\SaveAction::class,
          'delete'                => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\element\DeleteAction::class,
          'stopdelete'            => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\element\StopDeleteAction::class,
          'updateblock'          => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\element\UpdateBlockAction::class,
          'get'                   => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\element\GetAction::class,
          "network"               => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\element\NetworkAction::class,
      );
  }
}
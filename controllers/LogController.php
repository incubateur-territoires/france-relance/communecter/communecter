<?php

namespace PixelHumain\PixelHumain\modules\communecter\controllers;

use CommunecterController;

/**
 * LogController.php
 *
 * @author: Childéric THOREAU <childericthoreau@gmail.com>
 * Date: 7/29/15
 * Time: 12:25 AM
 */
class LogController extends CommunecterController {

	public function actions()
	{
	    return array(
	        'monitoring'    	 	 => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\log\MonitoringAction::class,
	        'cleanup'    			 => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\log\CleanUpAction::class,
	    );
	}
}
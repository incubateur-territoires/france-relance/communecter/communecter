<?php

namespace PixelHumain\PixelHumain\modules\communecter\controllers;

use CommunecterController;

/**
 * DiscussController.php
 *
 *
 * @author: Sylvain Barbot
 * Date: 24/06/2015
 */
class RoomsController extends CommunecterController {

    public function beforeAction($action) {
    	parent::initPage();
    	return parent::beforeAction($action);
  	}
  	public function actions()
	{
	    return array(
	        'index'       	=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\actionRoom\IndexAction::class,
			'saveroom'   	=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\actionRoom\SaveRoomAction::class,
			'deleteroom'   	=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\actionRoom\DeleteRoomAction::class,
			'editroom'		=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\actionRoom\EditRoomAction::class,
			'external'		=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\actionRoom\ExternalAction::class,
			'actions'		=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\actionRoom\ActionsAction::class,
			'action'		=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\actionRoom\ActionAction::class,
			'editaction'	=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\actionRoom\EditActionAction::class,
			'saveaction'	=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\actionRoom\SaveActionAction::class,
			'closeaction'	=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\actionRoom\CloseActionAction::class,
			'deleteaction'	=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\actionRoom\DeleteActionAction::class,
			'assignme'		=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\actionRoom\AssignMeAction::class,
			"fastaddaction" => 'citizenToolKit.controllers.actionRoom.FastAddActionAction',
			'move'	        => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\actionRoom\MoveAction::class,
	    );
	}
}
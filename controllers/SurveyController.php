<?php

namespace PixelHumain\PixelHumain\modules\communecter\controllers;

use CommunecterController;
use PixelHumain\PixelHumain\components\ThemeHelper;
use Yii;

/**
 * DefaultController.php
 *
 * OneScreenApp for Communecting people
 *
 * @author: Tibor Katelbach <tibor@pixelhumain.com>
 * Date: 14/03/2014
 */
class SurveyController extends CommunecterController {

    public function beforeAction($action)
  	{
      parent::initPage();
		  return parent::beforeAction($action);
  	}

    public function actions() {
      return array(
          'index'        => 'citizenToolKit.controllers.survey.IndexAction',
          'indexPod'     => 'citizenToolKit.controllers.survey.IndexAction',
          'entries'      => 'citizenToolKit.controllers.survey.EntriesAction',
          'entry'        => 'citizenToolKit.controllers.survey.EntryAction',
          'graph'        => 'citizenToolKit.controllers.survey.GraphAction',
          'savesession'  => 'citizenToolKit.controllers.survey.SaveSessionAction',
          'moderate'     => 'citizenToolKit.controllers.survey.ModerateAction',
          'delete'       => 'citizenToolKit.controllers.survey.DeleteAction',
          'close'        => 'citizenToolKit.controllers.survey.CloseAction',
          'addaction'    => 'citizenToolKit.controllers.action.AddActionAction',
          'editentry'    => 'citizenToolKit.controllers.survey.EditEntryAction',
          "fastaddentry" => 'citizenToolKit.controllers.survey.FastAddEntryAction',
      );
  }
  


  public function actionTextarea() 
  {
    ThemeHelper::setWebsiteTheme(ThemeHelper::EMPTY);
    return $this->render( "textarea" );
  }
  public function actionEditList() 
  {
    ThemeHelper::setWebsiteTheme(ThemeHelper::EMPTY);
    return $this->render( "editList" );
  }
  public function actionMultiAdd() 
  {
    ThemeHelper::setWebsiteTheme(ThemeHelper::EMPTY);
    return $this->render( "multiadd" );
  }
}
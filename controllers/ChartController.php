<?php

namespace PixelHumain\PixelHumain\modules\communecter\controllers;

use CommunecterController;

/**
 * ChartController.php
 *
 *
 * @author: Bouboule
 * Date: 24/06/2015
 */
class ChartController extends CommunecterController {

    public function beforeAction($action) {
    	parent::initPage();
    	return parent::beforeAction($action);
  	}
  	public function actions()
	{
	    return array(
	        'index'       		=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\chart\IndexAction::class,
			'addchartsv'       => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\chart\AddChartSvAction::class,
			'editchart'       => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\chart\EditChartAction::class,
			'get'       => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\chart\GetJsonAction::class,
	    );
	}
}
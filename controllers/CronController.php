<?php

namespace PixelHumain\PixelHumain\modules\communecter\controllers;

use CommunecterController;

/**
 * CronController.php
 *
 * @author: Sylvain Barbot <sylvain.barbot@gmail.com>
 * Date: 10/04/16
 * Time: 12:25 AM
 */
class CronController extends CommunecterController {
  

	public function beforeAction($action) {
	    parent::initPage();
	    return parent::beforeAction($action);
	}

	public function actions()
	{
	    return array(
	        'doCron'    			=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cron\DoCronAction::class,
	        'checkdeletepending'    => \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\cron\CheckDeletePendingAction::class,
	    );
	}
}
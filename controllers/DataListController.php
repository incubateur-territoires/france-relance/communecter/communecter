<?php

namespace PixelHumain\PixelHumain\modules\communecter\controllers;

use CommunecterController;
class DataListController extends CommunecterController {

	public function beforeAction($action) {
		parent::initPage();
		return parent::beforeAction($action);
	}

	public function actions() {
	    return array(
			'getlistbyname'			=> 'citizenToolKit.controllers.datalist.GetListByNameAction'
	    );
	}
}
<?php

namespace PixelHumain\PixelHumain\modules\communecter\controllers;

use CommunecterController;

/**
 * LinkController.php
 *
 * Manage Links between Organization, Person, Projet and Event
 *
 * @author: Sylvain Barbot <sylvain@pixelhumain.com>
 * Date: 05/05/2015
 */
class CollectionsController extends CommunecterController 
{
	public function beforeAction($action) {
		parent::initPage();
		return parent::beforeAction($action);
	}

	public function actions()
	{
	    return array(
	    	'add'     	=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\collections\AddAction::class,
	        'list'     	=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\collections\ListAction::class,
	        'crud'     	=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\collections\CrudAction::class,
	    );
	}
}
<?php

namespace PixelHumain\PixelHumain\modules\communecter\controllers;

use CommunecterController;

/**
 * EventController.php
 * 
 * contient tous ce qui concerne les utilisateurs / clietns TEEO
 *
 * @author: Tibor Katelbach <tibor@pixelhumain.com>
 * Date: 18/07/2014
 */
class GalleryController extends CommunecterController {

		public function beforeAction($action) {
			parent::initPage();
			return parent::beforeAction($action);
		}

	public function actions()
	{
	    return array(
	    	'index'       	=> \PixelHumain\PixelHumain\modules\citizenToolKit\controllers\gallery\IndexAction::class,
	        'getlistbyid'     	=> 'citizenToolKit.controllers.gallery.GetListByIdAction',
	    );
	}
}